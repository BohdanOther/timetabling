package lnu.timetable.algorithm;

import lnu.timetable.common.Matrix3D;

public class Rules {
    private String Break;
    private Matrix3D<Boolean> roomRule;
    private Matrix3D<Boolean> tchRule;
    private Matrix3D<Boolean> studRule;

    public Rules() {
        setBreak("");
    }

    public String getBreak() {
        return Break;
    }

    public void setBreak(String _break) {
        Break = _break;
    }

    public Matrix3D<Boolean> getRoom() {
        return roomRule;
    }

    public void setRoom(Matrix3D<Boolean> room) {
        System.out.println("in setRoom");
        this.roomRule = room;
        roomRule.fill(true);
    }

    public Matrix3D<Boolean> getTeacher() {
        return tchRule;
    }

    public void setTeacher(Matrix3D<Boolean> tch) {
        System.out.println("in setTeacher");
        this.tchRule = tch;
        tchRule.fill(true);
    }

    public Matrix3D<Boolean> getStudent() {
        return studRule;
    }

    public void setStudent(Matrix3D<Boolean> stud) {
        System.out.println("in setStudent");
        this.studRule = stud;
        studRule.fill(true);
    }
}
