package lnu.timetable.algorithm;


import lnu.timetable.common.Matrix2D;
import lnu.timetable.common.Matrix3D;
import lnu.timetable.common.Random;
import lnu.timetable.main.Main;
import lnu.timetable.models.Activity;
import lnu.timetable.models.AssignSubjects;
import lnu.timetable.models.Subject;
import lnu.timetable.persistence.context.TimetableDataContext;

import java.util.ArrayList;
import java.util.Vector;

public class TTGSGenerator {
    boolean ACTIVITY_SUCCESS = false;
    boolean GENERATE_SUCCESS = false;
    Matrix3D<Boolean> room;
    Matrix3D<Boolean> tch;
    Matrix3D<Boolean> stud;
    Matrix3D<String> studTimetable;
    Matrix3D<String> stud1Timetable;
    Vector<Boolean> isActPlace;
    Matrix2D<Integer> THPlaced;
    Matrix3D<Integer> PRPlaced;
    Matrix2D<String> ACTPlaced;
    private int r;
    private int c;
    private int tchLen;
    private int studLen;
    private int roomLen;
    private int subLen;
    private String Break, div;
    private TimetableDataContext ds;
    private ArrayList<AssignSubjects> assignSub;


    public void SetData(TimetableDataContext ds) {
        this.ds = ds;
        this.assignSub = ds.assignSubjectsDataSet.getAssignedSubjectList();

        tchLen = ds.teacherDao.length();
        studLen = ds.studentDao.getAllStudentLength();
        roomLen = ds.roomDao.length();
        subLen = ds.subjectDao.length();

        r = ds.getTime().getHour().size();
        c = ds.getTime().getDay().size();
        if (ds.getRule().getTeacher() == null)
            ds.getRule().setTeacher(new Matrix3D<>(tchLen, r, c));
        if (ds.getRule().getRoom() == null)
            ds.getRule().setRoom(new Matrix3D<>(roomLen, r, c));
        if (ds.getRule().getStudent() == null)
            ds.getRule().setStudent(new Matrix3D<>(studLen, r, c));
    }

    public void initMatrices() {
        System.out.println("fill is Start");
        GENERATE_SUCCESS = false;

        isActPlace = new Vector<>();
        for (int i = 0; i < ds.activityDao.length(); i++) {
            isActPlace.add(false);
        }

        THPlaced = new Matrix2D<>(subLen, c);
        PRPlaced = new Matrix3D<>(subLen, r, c);
        ACTPlaced = new Matrix2D<>(r, c);

        THPlaced.fill(0);
        PRPlaced.fill(0);
        ACTPlaced.fill("#");

        stud = new Matrix3D<>(studLen, r, c);
        tch = new Matrix3D<>(tchLen, r, c);
        room = new Matrix3D<>(roomLen, r, c);
        stud.fill(true);
        tch.fill(true);
        room.fill(true);

        studTimetable = new Matrix3D<>(studLen, r, c);

        // all resources available
        studTimetable.fill("[Empty]");

        Break = ds.getRule().getBreak();

        SetBusy(stud, ds.getRule().getStudent());
        SetBusy(tch, ds.getRule().getTeacher());
        SetBusy(room, ds.getRule().getRoom());

        SetBreaks(studTimetable, stud);

        System.out.println("fill is over");
    }

    private void SetBusy(Matrix3D<Boolean> A, Matrix3D<Boolean> B) {
        if (A.getPagesCount() == B.getPagesCount() && A.getRowsCount() == B.getRowsCount()
                && A.getColumnsCount() == B.getColumnsCount())
            for (int i = 0; i < A.getPagesCount(); i++)
                for (int j = 0; j < A.getRowsCount(); j++)
                    for (int k = 0; k < A.getColumnsCount(); k++) {
                        boolean t = B.getContent(i, j, k);
                        A.setContent(i, j, k, t);
                    }
    }

    private void SetBreaks(Matrix3D<String> A, Matrix3D<Boolean> B) {
        for (int T = 0; T < B.getPagesCount(); T++)
            for (int Days = 0; Days < B.getColumnsCount(); Days++)
                for (int TimeSlot = 0; TimeSlot < B.getRowsCount(); TimeSlot++)
                    if (ds.getTime().getHour().get(TimeSlot).getSpan()
                            .equalsIgnoreCase(Break)) {
                        A.setContent(T, TimeSlot, Days, "[ Break ]");
                        B.setContent(T, TimeSlot, Days, false);
                    }
    }

    public void Iteration(int iteration) {
        if (GENERATE_SUCCESS) {
            System.out.println("FIN" + iteration);
        } else {
            initMatrices();
            Random ActR = new Random(ds.activityDao.length());
            //  log += "\nStart Iteration >>" + "Iteration " + itration;

            for (int Days = 0; Days < c; Days++) {
                for (int TimeSlot = 0; TimeSlot < r; TimeSlot++) {
                    for (int activity = 0; activity < ds.activityDao.length(); activity++) {
                        // take random activity
                        // TODO: make some heuristic for smart activity choosing
                        int randAct = ActR.getRandValue(activity);
                        if (!isActPlace.get(randAct)) {
                            if (placeActivity(randAct, TimeSlot, Days)) {
                                isActPlace.set(randAct, true);
                               // Activity aa = ds.activityDao.get(randAct);
//                                log += "\n " + randAct + "[" + aa.getTeacher()
//                                        + "," + aa.getSubject() + ","
//                                        + aa.getStudent() + "," + aa.getTag()
//                                        + "]" + "(" + TimeSlot + ":" + Days
//                                        + ")";
                            }
                        }
                    }
                }
            }
            //log += "\nEnd Iteration >>";

            for (int i = 0; i < ds.activityDao.length(); i++) {
                int randAct = ActR.getRandValue(i);
                if (!isActPlace.get(randAct)) {
                    GENERATE_SUCCESS = false;
                    //log += "\nInteruppted at >>" + randAct;
                    break;
                } else {
                    GENERATE_SUCCESS = true;
                    createCombinedTT();
                }
            }

            if (GENERATE_SUCCESS) ACTPlaced.print();
        }

    }

    private boolean placeActivity(int actnum, int TimeSlot, int Days) {
        int T, S, R = -1, subno, dur;
        String tag, tname, sname, roomname, subname;
        String stemp, stemp1;

        dur = ds.activityDao.get(actnum).getDuration();
        tag = ds.activityDao.get(actnum).getTag();
        subname = ds.activityDao.get(actnum).getSubject();
        subno = ds.subjectDao.search(subname);

        tname = ds.activityDao.get(actnum).getTeacher();
        T = ds.teacherDao.search(tname);

        sname = ds.activityDao.get(actnum).getStudent();
        S = searchStudent(sname);

        roomname = "";
        for (int i = 0; i < ds.roomDao.length(); i++) {
            roomname = ds.roomDao.get(i).getName();
            String type = ds.roomDao.get(i).getType();

            if (tag.equals("TH") && type.equals("CLASS")) {
                R = ds.roomDao.search(roomname);
                if (isRoomAvail(R, TimeSlot, Days))
                    break;
            } else if ((tag.equals("PR") || tag.equals("TUT"))
                    && type.equals("LAB")) {// Pract =2
                R = ds.roomDao.search(roomname);
                if (isRoomAvail(R, TimeSlot, Days))
                    break;
            }
        }

        div = " : ";
        stemp = roomname + "\n(" + tname + div + subname + ") " ;
        stemp1 = roomname + "\n(" + tname + div + subname + ") " ;

        boolean TSR = isTeachAvail(T, TimeSlot, Days)
                && isStudAvail(S, TimeSlot, Days)
                && isRoomAvail(R, TimeSlot, Days);

        if (TSR) {
            if (tag.equals("TH")) {
                int tH = THPlaced.getContent(subno, Days);
                if (tH < dur) {
                    THPlaced.setContent(subno, Days, tH + dur);
                } else {
                    return false;
                }
                setStudUnAvail(S, TimeSlot, Days, actnum);
                studTimetable.setContent(S, TimeSlot, Days, stemp);
                tch.setContent(T, TimeSlot, Days, false);
                room.setContent(R, TimeSlot, Days, false);

                return true;
            } else {// Pract =2


                if (!isLast(TimeSlot)) {
                    boolean TSR1 = isTeachAvail(T, TimeSlot + 1, Days)
                            && isStudAvail(S, TimeSlot + 1, Days)
                            && isRoomAvail(R, TimeSlot + 1, Days);

                    if (!isBreak(TimeSlot) && TSR1) {
                        int pR = PRPlaced.getContent(subno, TimeSlot, Days);
                        if (pR < dur) {
                            PRPlaced.setContent(subno, TimeSlot, Days, pR + dur);
                        } else {
                            return false;
                        }

                        setStudUnAvail(S, TimeSlot, Days, actnum);
                        studTimetable.setContent(S, TimeSlot, Days, stemp);
                        studTimetable.setContent(S, TimeSlot + 1, Days, stemp1);

                        tch.setContent(T, TimeSlot, Days, false);
                        tch.setContent(T, TimeSlot + 1, Days, false);

                        room.setContent(R, TimeSlot, Days, false);
                        room.setContent(R, TimeSlot + 1, Days, false);

                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } else
            return false;

    }

    private boolean isLast(int timeSlot) {
        return r - 1 == timeSlot;
    }

    private boolean isBreak(int TimeSlot) {
        return ds.getTime().getHour().get(TimeSlot + 1).getSpan().equalsIgnoreCase(Break);
    }

    public int searchStudent(String sname) {
        int S = 0;
        for (int i = 0; i < ds.studentDao.getAllStudentLength(); i++) {
            if ( Main.context.studentDao.getStudentNames().get(i).equals(sname)) {
                S = i;
                break;
            }
        }
        return S;
    }

    private void setStudUnAvail(int s, int timeSlot, int days, int actnum) {
        int size = 0;
        int gsize;
        for (int i = 0; i < ds.studentDao.length(); i++) {
            gsize = ds.studentDao.get(i).getGroupList().size();
            size += gsize + 1;
            if (s < size) {
                if (ds.activityDao.get(actnum).getTag().equals("TH")) {
                    for (; s < size; s++)
                        stud.setContent(s, timeSlot, days, false);
                    break;
                } else {
                    int parent = size - gsize - 1;
                    stud.setContent(parent, timeSlot, days, false);
                    stud.setContent(parent, timeSlot + 1, days, false);
                    stud.setContent(s, timeSlot, days, false);
                    stud.setContent(s, timeSlot + 1, days, false);

                    setOtherStudUnAvail(parent, timeSlot, days);
                    break;
                }
            }
        }
    }

    private void setOtherStudUnAvail(int s, int timeSlot, int days) {
        int size = 0;
        int gsize;
        for (int i = 0; i < ds.studentDao.length(); i++) {
            if (size == s) {
                gsize = ds.studentDao.get(i).getGroupList().size();
                size += gsize + 1;
            } else {
                gsize = ds.studentDao.get(i).getGroupList().size();
                for (int z = 0; z < gsize; z++) {
                    int OtherS = size + z + 1;
                    stud.setContent(OtherS, timeSlot, days, false);
                    stud.setContent(OtherS, timeSlot + 1, days, false);
                }
                size += gsize + 1;
            }
        }
    }

    private boolean isStudAvail(int i, int j, int k) {
        return stud.getContent(i, j, k);
    }

    private boolean isTeachAvail(int i, int j, int k) {
        System.out.println(tch.getContent(i, j, k).toString());
        return tch.getContent(i, j, k);

    }

    private boolean isRoomAvail(int i, int j, int k) {
        if (i >= 0) {
            return room.getContent(i, j, k);
        } else
            return false;
    }

    public Matrix2D<String> getStud1Timetable(int i) {
        Matrix2D<String> TimeTable;
        TimeTable = stud1Timetable.get2DPart(i);
        return TimeTable;
    }

    public boolean isAllActivitiesPlace() {
        return GENERATE_SUCCESS;
    }

    public boolean isActivitiesGenerated() {
        return ACTIVITY_SUCCESS;
    }

    public void GenerateActivities() {
        int i = 0, TH = 0, PR = 0;
        int thour = 1; // dur theory
        int phour = 1; // dur practical
        if (ds.activityDao.getData().size() > 0)
            ds.activityDao.getData().clear();

        for (AssignSubjects anAssignSub : assignSub) {
            Subject temp = anAssignSub.getSubj();
            Activity activity;
            for (int ta = 0; ta < temp.getNoTheory(); ta++, i++) {
                activity = new Activity(i);
                activity.setTag("TH");
                activity.setStudent(anAssignSub.getStudTH());
                activity.setSubject(temp.getName());
                activity.setTeacher(anAssignSub.getTchTH());
                activity.setDuration(thour);
                TH++;
                ds.activityDao.addActivity(i, activity);
            }

            int n;
            int size = anAssignSub.getTchPR().size();

            for (int m = 0; m < anAssignSub.getStudPR().size(); m++, i++) {
                if (size == 1)
                    n = 0;
                else
                    n = m / size;
                activity = new Activity(i);
                if (temp.getNoPractice() > 1)
                    activity.setTag("PR");
                else
                    activity.setTag("TUT");
                activity.setStudent(anAssignSub.getStudPR().get(m));
                activity.setTeacher(anAssignSub.getTchPR().get(n));
                activity.setSubject(temp.getName());
                activity.setDuration(phour);
                PR++;
                ds.activityDao.addActivity(i, activity);
            }
        }
        ds.setTH(TH);
        ds.setPR(PR);

        ACTIVITY_SUCCESS = (ds.activityDao.getData().size() == (ds.getPR() + ds.getTH()))
                && (ds.activityDao.getData().size() != 0);
    }

    private void createCombinedTT() {
        int StudLen = ds.studentDao.length();
        stud1Timetable = new Matrix3D<>(StudLen, r, c);
        stud1Timetable.fill("[Empty]");

        for (int i = 0; i < StudLen; i++) {
            String sname = ds.studentDao.get(i).getName();

            for (int Days = 0; Days < c; Days++)

                for (int TimeSlot = 0; TimeSlot < r; TimeSlot++) {
                    int j = searchStudent(sname);

                    int lim = j + ds.studentDao.get(i).getGroupList().size();

                    String stemp = "";
                    for (; j < lim + 1; j++) {
                        String sname1 = Main.context.studentDao.getStudentNames().get(j);
                        String entry = studTimetable.getContent(j, TimeSlot, Days);

                        if (entry.contains("Break")) {
                            if (stemp.contains("Break"))
                                break;
                            stemp += "Break\n";
                        } else {

                            if (j == searchStudent(sname)) {
                                if (entry.contains("Empty")) {
                                    stemp = "Practice\n";
                                } else {
                                    stemp += "Theory\n";
                                    stemp += sname1 + " :" + entry + "\n";
                                    break;
                                }
                            } else {
                                if (entry.contains("Empty")) {
                                    stemp = "- x -";
                                } else {
                                    stemp += sname1 + " : " + entry + "\n";
                                }
                            }

                        }
                    }
                    stud1Timetable.setContent(i, TimeSlot, Days, stemp);
                }
        }
    }
}
