package lnu.timetable.algorithm;

import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import lnu.timetable.main.Main;
import lnu.timetable.ui.MessageBox;

/**
 * Created by Bohdan on 16-Dec-15.
 */
public class TimetableGenerationTask extends Task<Void> {

    @Override
    protected Void call() throws Exception {

        updateMessage("Initializing tables");


        int i = 0;
        int work = Main.context.activityDao.length() / 2;

        while (true) {
            if (isCancelled()) {
                break;
            }

            if (i == work || Main.generator.isAllActivitiesPlace()) {

                if (Main.generator.isAllActivitiesPlace()) {
                    updateMessage("Timetable Generated Successfully");
                } else {
                    updateMessage("Timetable not Generated !");
                }

                if (!Main.generator.isAllActivitiesPlace()) {

                    MessageBox.showAndWait("Error", "Timetable not Generated !", Alert.AlertType.INFORMATION);
                    done();
                    return null;
                }
                break;
            }

            updateMessage("Trying to put activity " + i);
            updateProgress(i, work);
            Main.generator.Iteration(i);

          //  Thread.sleep(1000);
            i++;
        }

        updateProgress(work, work);
        updateMessage("Time tables Generated! \n      Click to view");

        done();
        return null;
    }
}
