package lnu.timetable.common;

import java.util.Vector;

public class Matrix2D<T> {
    private int rowsCount;
    private int columnsCount;
    private Vector<T> Content;

    public Matrix2D(int R, int C) {
        this.rowsCount = R;
        this.columnsCount = C;
        Content = new Vector<>();
        Content.setSize(R * C);
    }

    public int getRowsCount() {
        return rowsCount;
    }

    public int getColumnsCount() {
        return columnsCount;
    }

    public void clear() {
        Content.clear();
    }

    public Vector<T> getContent() {
        return Content;
    }

    public Vector<T> rowSlice(int row) {
        Vector<T> slice = new Vector<>(columnsCount);

        for (int i = 0; i < columnsCount; i++) {
            slice.add(i, getContent(row, i));
        }
        return slice;
    }

    public T getContent(int i, int j) {
        return Content.get((i * columnsCount) + j);
    }

    public void setContent(int i, int j, T t) {
        Content.set((i * columnsCount) + j, t);
    }

    public void fill(T t) {
        clear();
        for (int i = 0; i < rowsCount * columnsCount; i++)
            getContent().add(i, t);
    }

    public void print() {
        for (int j = 0; j < columnsCount; j++) {
            for (int i = 0; i < rowsCount; i++)
                System.out.print(Content.get((i * columnsCount) + j) + "{//\\\\}");
            System.out.print("\n");
        }
    }
}
