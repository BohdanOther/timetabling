package lnu.timetable.common;

import java.util.Vector;

public class Matrix3D<T> {
    private int pagesCount;
    private int rowsCount;
    private int columnsCount;
    private Vector<T> Content;
    private T temp;

    public Matrix3D(int pages, int rows, int columns) {
        this.pagesCount = pages;
        this.rowsCount = rows;
        this.columnsCount = columns;
        Content = new Vector<>();
        Content.setSize(pages * rows * columns);
    }

    public T getT() {
        return temp;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public int getRowsCount() {
        return rowsCount;
    }

    public int getColumnsCount() {
        return columnsCount;
    }

    public void clearContent() {
        Content.clear();
    }

    public Vector<T> getContent() {
        return Content;
    }

    public T getContent(int i, int j, int k) {
        return Content.get((((i * rowsCount) + j) * columnsCount) + k);
    }

    public void setContent(int i, int j, int k, T t) {
        Content.set((((i * rowsCount) + j) * columnsCount) + k, t);
    }

    public void fill(T t) {
        this.temp = t;
        clearContent();
        for (int i = 0; i < pagesCount * rowsCount * columnsCount; i++)
            getContent().add(i, t);
    }

    public Matrix2D<T> get2DPart(int i) {
        Matrix2D<T> M2DForm;
        M2DForm = new Matrix2D<>(rowsCount, columnsCount);
        M2DForm.fill(getT());
        for (int j = 0; j < rowsCount; j++) {
            for (int k = 0; k < columnsCount; k++)
                M2DForm.setContent(j, k, getContent(i, j, k));
        }
        return M2DForm;
    }

    public void set2DPart(int i, Matrix2D<T> M2DForm) {
        for (int j = 0; j < M2DForm.getRowsCount(); j++)
            for (int k = 0; k < M2DForm.getColumnsCount(); k++)
                setContent(i, j, k, M2DForm.getContent(j, k));
    }
}

