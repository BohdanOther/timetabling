package lnu.timetable.io;


import lnu.timetable.main.Main;
import lnu.timetable.models.*;
import lnu.timetable.persistence.context.TimetableDataContext;
import lnu.timetable.persistence.dao.RoomDao;
import lnu.timetable.persistence.dao.StudentDao;
import lnu.timetable.persistence.dao.SubjectDao;
import lnu.timetable.persistence.dao.TeacherDao;
import org.dom4j.*;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.Iterator;
import java.util.List;

public class Import {

    public static void readData(String filename) {

        Document doc;

        TimetableDataContext context = Main.context;

        StudentDao studentDao = context.studentDao;
        TeacherDao teacherDao = context.teacherDao;
        SubjectDao subjectDao = context.subjectDao;
        RoomDao roomDao = context.roomDao;


        try {
            List<Element> innerEle;
            List<Element> groupElements;
            doc = new SAXReader().read(new File(filename));

            XPath catPath = DocumentHelper.createXPath("//Teachers_List/Teacher");
            List<Element> catElements = catPath.selectNodes(doc);
            for (Element element : catElements) {
                teacherDao.add(new Teacher(element.elementText("Name"),
                        element.elementText("Type")));
            }

            catPath = DocumentHelper.createXPath("//Subjects_List/Subject");
            catElements = catPath.selectNodes(doc);
            for (Element element : catElements) {
                subjectDao.add(new Subject(element.elementText("Name"),
                        Integer.parseInt(element.elementText("No_Theory")),
                        Integer.parseInt(element.elementText("No_Practical"))));
            }

            catPath = DocumentHelper.createXPath("//Students_List/Student");
            catElements = catPath.selectNodes(doc);
            for (Element element : catElements) {
                studentDao.add(new Student(element.elementText("Name"),
                        Integer.parseInt(element.elementText("Strength"))));

                if (element.element("Group") != null) {
                    groupElements = element.elements("Group");
                    for (Element Gelement : groupElements) {
                          context.studentDao.addGroup(element.elementText("Name"), Gelement.elementText("Name"), Integer.parseInt(Gelement.elementText("Strength")));
                    }
                }
            }

            catPath = DocumentHelper.createXPath("//Subjects_Alloc_List/SubjectAlloc");
            catElements = catPath.selectNodes(doc);

            if (context.assignSubjectsDataSet.getAssignedSubjectLength() < context.subjectDao.length())
                context.assignSubjectsDataSet.copySubjToAssignSubj();
            int subjlen = 0;
            int PR = 0, TH = 0;
            String s;
            for (Iterator<Element> it = catElements.iterator(); it.hasNext(); subjlen++) {
                Element element = it.next();

                if (element.element("Theory") != null) {
                    TH += context.subjectDao.get(subjlen).getNoTheory();
                    Element Nelement = element.element("Theory");
                    s = Nelement.elementText("Teacher");
                    context.assignSubjectsDataSet.getAssignedSubject(subjlen).setTchTH(s);

                    s = Nelement.elementText("Student");
                    context.assignSubjectsDataSet.getAssignedSubject(subjlen).setStudTH(s);
                }

                if (element.element("Practical") != null) {
                    groupElements = element.elements("Practical");
                    for (Element Nelement : groupElements) {
                        innerEle = Nelement.elements("Teacher");
                        for (Element anInnerEle1 : innerEle) {
                            s = anInnerEle1.getText();
                            context.assignSubjectsDataSet.getAssignedSubject(subjlen).getTchPR().add(s);
                        }
                        innerEle = Nelement.elements("Student");
                        for (Element anInnerEle : innerEle) {
                            PR++;
                            s = anInnerEle.getText();
                            context.assignSubjectsDataSet.getAssignedSubject(subjlen).getStudPR().add(s);
                        }
                    }
                }
            }
            context.setTH(TH);
            context.setPR(PR);
            System.out.println(Main.context.getPR() + ":0:" + Main.context.getTH());

            catPath = DocumentHelper.createXPath("//Rooms_List/Room");
            catElements = catPath.selectNodes(doc);
            for (Element element : catElements) {
                roomDao.add(new Room(element.elementText("Name"),
                        Integer.parseInt(element.elementText("Capacity")),
                        element.elementText("Type")));
            }

            Main.context.getTime().getDay().clear();
            catPath = DocumentHelper.createXPath("//Days_List/Day");
            catElements = catPath.selectNodes(doc);
            for (Element element : catElements) {
                Main.context.getTime().getDay().add(
                        element.elementText("Name"));
            }

            catPath = DocumentHelper.createXPath("//Hours_List/Hour");
            catElements = catPath.selectNodes(doc);
            for (Element element : catElements) {
                Main.context.getTime().getHour().add(
                        new TimeSpan(element.elementText("Name")));
            }

        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

}
