package lnu.timetable.io;


import lnu.timetable.data.DataType;
import lnu.timetable.models.*;
import lnu.timetable.persistence.context.TimetableDataContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


public class Export {
    public static final String ext = ".xml";
    DocumentBuilderFactory documentBuilderFactory;
    DocumentBuilder documentBuilder;
    Document document;

    Element rootElement;
    Element dayElement;
    Element hourElement;
    Element teacherElement;
    Element subjectElement;
    Element subjectAllocElement;
    Element studentElement;
    Element roomElement;
    Element activityElement;


    public void NewKB() {
        try {
            documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.newDocument();
            rootElement = document.createElement("TTGS");
            dayElement = document.createElement("Days_List");
            hourElement = document.createElement("Hours_List");
            teacherElement = document.createElement("Teachers_List");
            subjectElement = document.createElement("Subjects_List");
            subjectAllocElement = document.createElement("Subjects_Alloc_List");
            studentElement = document.createElement("Students_List");
            roomElement = document.createElement("Rooms_List");
            activityElement = document.createElement("Activity_List");
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void SaveKB(String XMLFile, TimetableDataContext ds) {
        NewKB();

        for (int i = 0; i < ds.getTime().getDay().size(); i++) {
            Element em = document.createElement("Day");
            em.appendChild(AddElement(ds.getTime().getDay().get(i), DataType.DATA_NAME));
            AddKBElement("Day", em);
        }
        for (int i = 0; i < ds.getTime().getHour().size(); i++) {
            Element em = document.createElement("Hour");
            em.appendChild(AddElement(ds.getTime().getHour().get(i).getSpan(), DataType.DATA_NAME));
            AddKBElement("Hour", em);
        }
        for (int i = 0; i < ds.teacherDao.length(); i++) {
            Teacher temp = ds.teacherDao.get(i);
            AddKBElement("Teacher", AddTeacherKB(temp));
        }
        for (int i = 0; i < ds.subjectDao.length(); i++) {
            Subject temp = ds.subjectDao.get(i);
            AddKBElement("Subject", AddSubjectKB(temp));
        }
        for (int i = 0; i < ds.studentDao.length(); i++) {
            Student temp = ds.studentDao.get(i);
            AddKBElement("Student", AddStudentKB(temp, true));
        }
        for (int i = 0; i < ds.roomDao.length(); i++) {
            Room temp = ds.roomDao.get(i);
            AddKBElement("Room", AddRoomKB(temp));
        }
        for (int i = 0; i < ds.assignSubjectsDataSet.getAssignedSubjectLength(); i++) {
            AssignSubjects temp = ds.assignSubjectsDataSet.getAssignedSubject(i);
            AddKBElement("SubjectAlloc", AddSubjectAllocKB(temp));
        }
        for (int i = 0; i < ds.activityDao.length(); i++) {
            Activity temp = ds.activityDao.get(i);
            AddKBElement("Activity", AddActivityKB(temp));
        }

        try {
            rootElement.appendChild(dayElement);
            rootElement.appendChild(hourElement);
            rootElement.appendChild(studentElement);
            rootElement.appendChild(teacherElement);
            rootElement.appendChild(subjectElement);
            rootElement.appendChild(roomElement);
            rootElement.appendChild(subjectAllocElement);
            rootElement.appendChild(activityElement);

            document.appendChild(rootElement);

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(document);

            if (XMLFile.endsWith(ext)) {
                StreamResult result = new StreamResult(XMLFile);
                transformer.transform(source, result);
            } else {
                StreamResult result = new StreamResult(XMLFile + ext);
                transformer.transform(source, result);
            }
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }

    public Element AddTeacherKB(Teacher teacher) {
        Element em = document.createElement("Teacher");
        em.appendChild(AddElement(teacher.getName(), DataType.DATA_NAME));
        em.appendChild(AddElement(teacher.getType(), DataType.DATA_TYPE));
        return em;
    }

    public Element AddRoomKB(Room room) {
        Element em = document.createElement("Room");
        em.appendChild(AddElement(room.getName(), DataType.DATA_NAME));
        em.appendChild(AddElement(room.getType(), DataType.DATA_TYPE));
        em.appendChild(AddElement(String.valueOf(room.getCapacity()), DataType.DATA_CAPACITY));
        return em;
    }

    public Element AddSubjectKB(Subject subj) {
        Element em = document.createElement("Subject");
        em.appendChild(AddElement(subj.getName(), DataType.DATA_NAME));
        em.appendChild(AddElement(String.valueOf(subj.getNoTheory()), DataType.DATA_NOT));
        em.appendChild(AddElement(String.valueOf(subj.getNoPractice()), DataType.DATA_NOP));
        return em;
    }

    public Element AddSubjectAllocKB(AssignSubjects subj) {
        Element em = document.createElement("SubjectAlloc");
        Element em1 = document.createElement("Theory");
        Element em2 = document.createElement("Practical");

        em1.appendChild(AddElement(subj.getTchTH(), "Teacher"));
        em1.appendChild(AddElement(subj.getStudTH(), "Student"));

        for (int i = 0; i < subj.getTchPR().size(); i++)
            em2.appendChild(AddElement(subj.getTchPR().get(i), "Teacher"));
        for (int i = 0; i < subj.getStudPR().size(); i++)
            em2.appendChild(AddElement(subj.getStudPR().get(i), "Student"));

        em.appendChild(AddElement(subj.getSubj().getName(), DataType.DATA_NAME));

        if (subj.getSubj().getNoTheory() > 0)
            em.appendChild(em1);
        if (subj.getSubj().getNoPractice() > 0)
            em.appendChild(em2);
        return em;
    }

    public Element AddActivityKB(Activity act) {
        Element em = document.createElement("Activity");
        em.setAttribute("ID", String.valueOf(act.getId()));
        em.setAttribute("Tag", act.getTag());
        em.setAttribute("Subject", act.getSubject());
        em.setAttribute("Student", act.getStudent());
        em.setAttribute("Teacher", act.getTeacher());
        em.setAttribute("Duration", String.valueOf(act.getDuration()));

        String roomName;
        if (act.getTag().equals("TH")) {
            roomName = act.getHomeRoom();
            em.appendChild(AddElement(roomName, "Room"));
        } else if (act.getTag().equals("PR")) {
            for (int i = 0; i < act.getRooms().size(); i++) {
                roomName = act.getRooms().get(i);
                em.appendChild(AddElement(roomName, "Room"));
            }
        }
        return em;
    }

    public Element AddStudentKB(Student stud, boolean b) {
        Element em = document.createElement("Student");
        em.appendChild(AddElement(stud.getName(), DataType.DATA_NAME));
        em.appendChild(AddElement(String.valueOf(stud.getTotStud()), DataType.DATA_STRENGTH));
        if (b)
            for (int j = 0; j < stud.getGroupList().size(); j++) {
                Student gtemp = stud.getGroupList().get(j);
                Element group = document.createElement("Group");
                group.setAttribute("ID", String.valueOf(j));
                group.appendChild(AddElement(gtemp.getName(), DataType.DATA_NAME));
                group.appendChild(AddElement(String.valueOf(gtemp.getTotStud()), DataType.DATA_STRENGTH));
                em.appendChild(group);
            }
        return em;
    }

    public Element AddElement(String name, String caption) {
        Element em;
        if (name == null)
            name = "-Not alloacte yet-";
        em = document.createElement(caption);
        em.appendChild(document.createTextNode(name));
        return em;
    }

    public Element AddElement(String name, int data) {
        Element em;
        switch (data) {
            case DataType.DATA_NAME:
                em = document.createElement("Name");
                em.appendChild(document.createTextNode(name));
                return em;
            case DataType.DATA_TYPE:
                em = document.createElement("Type");
                em.appendChild(document.createTextNode(name));
                return em;
            case DataType.DATA_CAPACITY:
                em = document.createElement("Capacity");
                em.appendChild(document.createTextNode(name));
                return em;
            case DataType.DATA_NOT:
                em = document.createElement("No_Theory");
                em.appendChild(document.createTextNode(name));
                return em;
            case DataType.DATA_NOP:
                em = document.createElement("No_Practical");
                em.appendChild(document.createTextNode(name));
                return em;
            case DataType.DATA_STRENGTH:
                em = document.createElement("Strength");
                em.appendChild(document.createTextNode(name));
                return em;
            default:
        }
        return null;
    }

    public void AddKBElement(String ele, Element em) {
        if (ele.equalsIgnoreCase("teacher"))
            teacherElement.appendChild(em);
        else if (ele.equalsIgnoreCase("subject"))
            subjectElement.appendChild(em);
        else if (ele.equalsIgnoreCase("subjectalloc"))
            subjectAllocElement.appendChild(em);
        else if (ele.equalsIgnoreCase("student"))
            studentElement.appendChild(em);
        else if (ele.equalsIgnoreCase("room"))
            roomElement.appendChild(em);
        else if (ele.equalsIgnoreCase("day"))
            dayElement.appendChild(em);
        else if (ele.equalsIgnoreCase("hour"))
            hourElement.appendChild(em);
        else if (ele.equalsIgnoreCase("activity"))
            activityElement.appendChild(em);
    }

}
