package lnu.timetable.io;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfWriter;
import javafx.collections.ObservableList;
import lnu.timetable.algorithm.TTGSGenerator;
import lnu.timetable.common.Matrix2D;
import lnu.timetable.models.TimeSpan;
import lnu.timetable.persistence.context.TimetableDataContext;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Vector;

public class OutputGenerator {
    public static int PDF = 0;

    String path, cname, dname;
    String[] wDay = {"Monday", "Tuesday", "Wednesday", "Thrusday", "Friday"};
    String[] wHour = {"Hour 1", "Hour 2", "Hour 3", "Hour 4", "Hour 5",
            "Hour 6", "Hour 7", "Hour 8"};

    ArrayList<String> day;
    ObservableList<TimeSpan> hour;
    Vector<String> teacher;
    Vector<String> room;
    Vector<String> student;

    TimetableDataContext ds;
    TTGSGenerator ttgsGen;
    Matrix2D<String> TimeTable, empty;

    public OutputGenerator(String path) {
        this.path = path;
        empty = new Matrix2D<>(wHour.length, wDay.length);
        empty.fill("\n---X---\n");
    }

    public String getPlainEntry(String entry) {
        if (entry.contains("TUT")) {
            entry = entry.replace("TUT", "").replace("$", "");
        } else if (entry.contains("PR")) {
            entry = entry.replace("PR", "").replace("$", "");
        } else if (entry.contains("TH")) {
            entry = entry.replace("TH", "").replace("$", "");
        } else if (entry.contains("Break")) {
            entry = entry.replace("[", "").replace("]", "");
        } else {
            entry = entry.replace("[", "").replace("]", "");
        }
        return entry;
    }

    public void toPDF(String fname, Matrix2D<String> tt) {
        File f = new File(path + "/PDFTABLE/" + fname + ".pdf");
        File dir = new File(f.getParent());
        if (!dir.isDirectory()) {
            // System.out.println(dir.getPath() + "is Not Avail");
            if (dir.mkdir())
                System.out.println("Created");
        }
        try {
            Document document = new Document(PageSize._11X17, 50, 50, 50, 50);
            PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream(f));
            document.open();
            Paragraph title1 = new Paragraph(fname + " Timetable", FontFactory
                    .getFont(FontFactory.HELVETICA, 18, Font.BOLDITALIC,
                            new Color(0, 0, 255)));

            Chapter chapter1 = new Chapter(title1, 1);
            chapter1.setNumberDepth(0);
            Section section1 = chapter1.addSection(title1);
            section1.add(HorizontalTable(tt));
            document.add(chapter1);
            document.close();
        } catch (Exception e2) {
            // MSGBOX("Fail to create", e2.getMessage());
        }
    }

    public Table HorizontalTable(Matrix2D<String> tt) {
        Table t1;
        Table t2;
        Table tm;

        try {
            t1 = new Table(day.size() + 1, 1);
            t2 = new Table(day.size() + 1, hour.size());
            t1.setAlignment(Table.ALIGN_CENTER);
            t2.setAlignment(Table.ALIGN_CENTER);

            for (int j = 0; j < day.size() + 1; j++) {
                t1.setCellsFitPage(true);
                if (j == 0)
                    t1.addCell(new Cell("Time/Day"));
                else
                    t1.addCell(new Cell(day.get(j - 1)));
            }

            for (int j = 0; j < hour.size(); j++) {
                for (int i = 0; i < day.size() + 1; i++) {
                    t2.setCellsFitPage(true);
                    if (i == 0)
                        t2.addCell(hour.get(j).getSpan());
                    else {
                        String entry = tt.getContent(j, i - 1);
                        if (entry.contains("~")) {
                            entry = entry.replace("~", "");
                            entry = getPlainEntry(entry);
                        } else {
                            entry = getPlainEntry(entry);
                        }
                        t2.addCell(entry);
                    }
                }
            }

            tm = new Table(1, 3);
            tm.setAlignment(Table.ALIGN_CENTER);
            tm.setBorderWidth(1);
            tm.setPadding(5);
            tm.setSpacing(5);
            tm.addCell(new Cell(cname + "\n " + dname));
            tm.insertTable(t1);
            tm.insertTable(t2);
            return tm;
        } catch (BadElementException e) {
            e.printStackTrace();
        }
        return null;
    }
}
