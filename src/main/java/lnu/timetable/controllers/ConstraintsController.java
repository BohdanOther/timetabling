package lnu.timetable.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import lnu.timetable.common.Matrix2D;
import lnu.timetable.common.Matrix3D;
import lnu.timetable.main.Main;
import lnu.timetable.models.*;
import lnu.timetable.ui.MessageBox;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Bohdan on 17-Dec-15.
 */
public class ConstraintsController implements Initializable {

    @FXML
    private TableView<ConstraintEntry> tblTeacherTimeConstraints;
    @FXML
    private TableView<ConstraintEntry> tblStudentTimeConstraints;
    @FXML
    private TableView<ConstraintEntry> tblRoomTimeConstraints;

    @FXML
    private ComboBox<Teacher> cbTeacher;
    @FXML
    private ComboBox<Student> cbStudents;
    @FXML
    private ComboBox<Room> cbRooms;
    @FXML
    private ComboBox<TimeSpan> cbBreaks;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cbTeacher.setItems(Main.context.teacherDao.getData());
        cbStudents.setItems(Main.context.studentDao.getData());
        cbRooms.setItems(Main.context.roomDao.getData());
        cbBreaks.setItems(Main.context.getTime().getHour());

        cbTeacher.getSelectionModel()
                .selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> fillTeacherConstrainTable(newValue));

        cbStudents.getSelectionModel()
                .selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> fillStudentConstrainTable(newValue));

        cbRooms.getSelectionModel()
                .selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> fillRoomConstrainTable(newValue));
    }

    private void fillRoomConstrainTable(Room room) {
        if (room == null) {
            return;
        }

        int s = Main.context.roomDao.search(room.getName());
        Matrix2D<Boolean> timeTable = Main.context.getRule().getRoom().get2DPart(s);

        ObservableList<ConstraintEntry> constraintEntries = FXCollections.observableArrayList();
        for (int i = 0; i < timeTable.getRowsCount(); i++) {
            ConstraintEntry entry = new ConstraintEntry(Main.context.getTime().getHour().get(i).getSpan(), timeTable.rowSlice(i));
            constraintEntries.add(entry);
        }

        tblRoomTimeConstraints.setItems(constraintEntries);

        for (int i = 1; i < tblRoomTimeConstraints.getColumns().size(); i++) {
            TableColumn c = tblRoomTimeConstraints.getColumns().get(i);
            c.setVisible(Main.context.getTime().getDay().contains(c.getText()));
        }
    }

    private void fillStudentConstrainTable(Student student) {
        if (student == null) {
            return;
        }

        int s = Main.context.studentDao.search(student.getName());
        Matrix2D<Boolean> timeTable = Main.context.getRule().getStudent().get2DPart(s);

        ObservableList<ConstraintEntry> constraintEntries = FXCollections.observableArrayList();
        for (int i = 0; i < timeTable.getRowsCount(); i++) {
            ConstraintEntry entry = new ConstraintEntry(Main.context.getTime().getHour().get(i).getSpan(), timeTable.rowSlice(i));
            constraintEntries.add(entry);
        }

        tblStudentTimeConstraints.setItems(constraintEntries);

        for (int i = 1; i < tblStudentTimeConstraints.getColumns().size(); i++) {
            TableColumn c = tblStudentTimeConstraints.getColumns().get(i);
            c.setVisible(Main.context.getTime().getDay().contains(c.getText()));
        }
    }

    private void fillTeacherConstrainTable(Teacher teacher) {
        if (teacher == null) {
            return;
        }

        int s = Main.context.teacherDao.search(teacher.getName());
        Matrix2D<Boolean> timeTable = Main.context.getRule().getTeacher().get2DPart(s);

        ObservableList<ConstraintEntry> constraintEntries = FXCollections.observableArrayList();
        for (int i = 0; i < timeTable.getRowsCount(); i++) {
            ConstraintEntry entry = new ConstraintEntry(Main.context.getTime().getHour().get(i).getSpan(), timeTable.rowSlice(i));
            constraintEntries.add(entry);
        }

        tblTeacherTimeConstraints.setItems(constraintEntries);

        for (int i = 1; i < tblTeacherTimeConstraints.getColumns().size(); i++) {
            TableColumn c = tblTeacherTimeConstraints.getColumns().get(i);
            c.setVisible(Main.context.getTime().getDay().contains(c.getText()));
        }
    }

    public void onTeacherSetRulesAction(ActionEvent actionEvent) {
        Teacher teacher = cbTeacher.getValue();

        if (teacher == null) {
            MessageBox.showAndWait("Error", "Select teacher first", Alert.AlertType.WARNING);
            return;
        }
        int index = Main.context.teacherDao.search(teacher.getName());

        Matrix3D<Boolean> teacherRules = Main.context.getRule().getTeacher();

        int rows = teacherRules.getRowsCount();
        int cols = teacherRules.getColumnsCount();

        Matrix2D<Boolean> constraints = new Matrix2D<>(rows, cols);
        for (int i = 0; i < rows; i++) {
            ConstraintEntry e = tblTeacherTimeConstraints.getItems().get(i);
            for (int j = 0; j < cols; j++) {
                String col = tblTeacherTimeConstraints.getColumns().get(j + 1).getText();
                constraints.setContent(i, j, e.getByNumber(j));
            }
        }

        Main.context.getRule().getTeacher().set2DPart(index, constraints);

        MessageBox.showAndWait("Done", "Constraints set successfully", Alert.AlertType.INFORMATION);
    }

    public void onStudentSetRulesAction(ActionEvent actionEvent) {
        Student student = cbStudents.getValue();

        if (student == null) {
            MessageBox.showAndWait("Error", "Select student first", Alert.AlertType.WARNING);
            return;
        }
        int index = Main.context.studentDao.search(student.getName());

        Matrix3D<Boolean> studentRules = Main.context.getRule().getStudent();

        int rows = studentRules.getRowsCount();
        int cols = studentRules.getColumnsCount();

        Matrix2D<Boolean> constraints = new Matrix2D<>(rows, cols);
        for (int i = 0; i < rows; i++) {
            ConstraintEntry e = tblStudentTimeConstraints.getItems().get(i);
            for (int j = 0; j < cols; j++) {
                constraints.setContent(i, j, e.getByNumber(j));
            }
        }

        Main.context.getRule().getStudent().set2DPart(index, constraints);

        MessageBox.showAndWait("Done", "Constraints set successfully", Alert.AlertType.INFORMATION);
    }

    public void onRoomSetRulesAction(ActionEvent actionEvent) {
        Room room = cbRooms.getValue();

        if (room == null) {
            MessageBox.showAndWait("Error", "Select student first", Alert.AlertType.WARNING);
            return;
        }
        int index = Main.context.roomDao.search(room.getName());

        Matrix3D<Boolean> roomRules = Main.context.getRule().getRoom();

        int rows = roomRules.getRowsCount();
        int cols = roomRules.getColumnsCount();

        Matrix2D<Boolean> constraints = new Matrix2D<>(rows, cols);
        for (int i = 0; i < rows; i++) {
            ConstraintEntry e = tblRoomTimeConstraints.getItems().get(i);
            for (int j = 0; j < cols; j++) {
                constraints.setContent(i, j, e.getByNumber(j));
            }
        }

        Main.context.getRule().getRoom().set2DPart(index, constraints);

        MessageBox.showAndWait("Done", "Constraints set successfully", Alert.AlertType.INFORMATION);
    }

    public void onBtnSetBreakAction(ActionEvent actionEvent) {
        TimeSpan span = cbBreaks.getValue();

        if(span == null) return;

        Main.context.getRule().setBreak(span.getSpan());
        MessageBox.showAndWait("Done", "Break set successfully", Alert.AlertType.CONFIRMATION);
    }
}
