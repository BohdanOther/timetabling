package lnu.timetable.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import lnu.timetable.algorithm.TimetableGenerationTask;
import lnu.timetable.common.Matrix2D;
import lnu.timetable.main.Main;
import lnu.timetable.models.Student;
import lnu.timetable.models.Time;
import lnu.timetable.models.TimetableEntry;
import lnu.timetable.ui.MessageBox;
import org.controlsfx.control.MaskerPane;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Bohdan on 16-Dec-15.
 */
public class TimetablesController implements Initializable {

    private TimetableGenerationTask task = new TimetableGenerationTask();
    private ExecutorService executorService = Executors.newCachedThreadPool();

    private MaskerPane masker;

    @FXML
    private TableView<TimetableEntry> timetable;
    @FXML
    private StackPane stackPane;
    @FXML
    private ComboBox<Student> comboBoxTimetableId;

    public void onBtnGenerateAction(ActionEvent actionEvent) {

        if(!Main.generator.isActivitiesGenerated()){
            MessageBox.showAndWait("Error", "No Activities Set", Alert.AlertType.ERROR);
            return;
        }

        Main.generator.initMatrices();
        task = new TimetableGenerationTask();

        // add to the UI
        masker.progressProperty().bind(task.progressProperty());
        masker.textProperty().bind(task.messageProperty());
        masker.setProgressVisible(true);

        // execute task
        executorService.submit(task);
    }

    private void fillTimeTable(Student student) {

        int id = Main.context.studentDao.search(student.getName());

        Matrix2D<String> timeTable = Main.generator.getStud1Timetable(id);

        ObservableList<TimetableEntry> list = FXCollections.observableArrayList();
        Time time = Main.context.getTime();

        for (int i = 0; i < timeTable.getRowsCount(); i++) {
            TimetableEntry entry = new TimetableEntry(time.getHour().get(i).getSpan(), timeTable.rowSlice(i));
            list.add(entry);
        }
        timetable.setItems(list);

        for(int i = 1; i <timetable.getColumns().size(); i++){
            TableColumn c = timetable.getColumns().get(i);
            c.setVisible(time.getDay().contains(c.getText()));
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        stackPane.getChildren().clear();

        masker = new MaskerPane();
        masker.setVisible(true);
        masker.setText("No time tables generated yet");
        masker.setProgressVisible(false);

        stackPane.getChildren().add(masker);

        comboBoxTimetableId.setDisable(true);

        comboBoxTimetableId.setItems(Main.context.studentDao.getData());

        comboBoxTimetableId.getSelectionModel()
                .selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> fillTimeTable(newValue));

        masker.setOnMouseClicked(event -> {
            if (task.isDone() && Main.generator.isAllActivitiesPlace()) {
                masker.setVisible(false);
                comboBoxTimetableId.setDisable(false);
                timetable.setPlaceholder(new Label("Choose timetable from students list"));

                stackPane.getChildren().clear();
                stackPane.getChildren().add(timetable);
            }
        });
    }
}

