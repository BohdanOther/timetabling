package lnu.timetable.controllers.dataset;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import lnu.timetable.main.Main;
import lnu.timetable.models.Subject;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Bohdan on 15-Dec-15.
 */
public class SubjectController implements Initializable {

    @FXML
    private TableView<Subject> tblSubjects;
    @FXML
    private TextField tbSubjectName;
    @FXML
    private TextField tbSubjectTheory;
    @FXML
    private TextField tbSubjectPractice;

    @FXML
    public void onAddSubjectAction(ActionEvent event) {
        try {
            String name = tbSubjectName.getText();
            int theory = Integer.parseInt(tbSubjectTheory.getText());
            int practice = Integer.parseInt(tbSubjectPractice.getText());

            Subject subject = new Subject(name, theory, practice);
            tblSubjects.getItems().add(subject);
        } catch (Exception e) {
            Alert dlg = new Alert(Alert.AlertType.ERROR, "");
            dlg.setTitle("Wrong input");
            dlg.getDialogPane().setContentText("Subject information was incorrect");

            dlg.showAndWait();
        }
        tbSubjectName.clear();
        tbSubjectTheory.clear();
        tbSubjectPractice.clear();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tblSubjects.setItems(Main.context.subjectDao.getData());
    }
}
