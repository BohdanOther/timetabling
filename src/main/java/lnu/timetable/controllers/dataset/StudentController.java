package lnu.timetable.controllers.dataset;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import lnu.timetable.main.Main;
import lnu.timetable.models.Student;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Bohdan on 15-Dec-15.
 */
public class StudentController implements Initializable {

    @FXML
    private TableView<Student> tblStudents;
    @FXML
    private TextField tbStudentName;
    @FXML
    private TextField tbStudentCount;

    @FXML
    public void onAddStudentAction(ActionEvent event) {
        try {
            String name = tbStudentName.getText();
            int count = Integer.parseInt(tbStudentCount.getText());

            Student student = new Student(name, count);
            tblStudents.getItems().add(student);
            /* or group!
            } else if (addBtn.getText().equals("Add Group")) {
                if (!(P0.validate(name, 0) && P0.validate(gname, 0) && P0.validate(strength, 0))) {
                    JOptionPane.showMessageDialog(null, "Fill Empty Fields!", "Empty", JOptionPane.INFORMATION_MESSAGE);
                } else if (!P0.validate(strength, 1)) {
                    JOptionPane.showMessageDialog(null, "Field is a numeric!", "Number", JOptionPane.ERROR_MESSAGE);
                } else {
                    if (getTTGSData().studentDataSet.addGroup(name.getText(), gname.getText(), Integer.parseInt(strength.getText()))) {
             */
        } catch (Exception e) {
            Alert dlg = new Alert(Alert.AlertType.ERROR, "");
            dlg.setTitle("Wrong input");
            dlg.getDialogPane().setContentText("Student information was incorrect");

            dlg.showAndWait();
        }
        tbStudentCount.clear();
        tbStudentName.clear();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tblStudents.setItems(Main.context.studentDao.getData());
    }
}
