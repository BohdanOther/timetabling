package lnu.timetable.controllers.dataset;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import lnu.timetable.main.Main;
import lnu.timetable.models.Room;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Bohdan on 15-Dec-15.
 */
public class RoomController implements Initializable {

    @FXML
    private TableView<Room> tblRooms;
    @FXML
    private TextField tbRoomName;
    @FXML
    private TextField tbRoomCapacity;
    @FXML
    private ComboBox<String> cboxRoomType;

    @FXML
    public void onAddRoomAction(ActionEvent event) {
        // final JComboBox jcbATag = new JComboBox();
        // jcbATag.setModel(new DefaultComboBoxModel(new String[]{"Select Room", "CLASS", "LAB"}));
        try {
            String name = tbRoomName.getText();

            if (cboxRoomType.getSelectionModel().isEmpty()) {
                throw new Exception("Room type not selected");
            }
            String type = cboxRoomType.getSelectionModel().getSelectedItem();

            int capicity = Integer.parseInt(tbRoomCapacity.getText());

            Room room = new Room(name, capicity, type);
            tblRooms.getItems().add(room);
        } catch (Exception e) {
            Alert dlg = new Alert(Alert.AlertType.ERROR, "");
            dlg.setTitle("Wrong input");
            dlg.getDialogPane().setContentText("Room information was incorrect");

            dlg.showAndWait();
        }
        tbRoomName.clear();
        tbRoomCapacity.clear();
        cboxRoomType.getSelectionModel().clearSelection();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cboxRoomType.getItems().setAll("CLASS", "LAB");

        tblRooms.setItems(Main.context.roomDao.getData());
    }
}
