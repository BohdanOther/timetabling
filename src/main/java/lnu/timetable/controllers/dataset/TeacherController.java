package lnu.timetable.controllers.dataset;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import lnu.timetable.main.Main;
import lnu.timetable.models.Teacher;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Bohdan on 15-Dec-15.
 */
public class TeacherController  implements Initializable{

    @FXML
    private TableView<Teacher> tblTeachers;
    @FXML
    private TextField tbTeacherName;
    @FXML
    private TextField tbTeacherType;

    @FXML
    public void onAddTeacherAction(ActionEvent event) {
        try {
            String name = tbTeacherName.getText();
            String type = tbTeacherType.getText();

            Teacher teacher = new Teacher(name, type);
            tblTeachers.getItems().add(teacher);
        } catch (Exception e) {
            Alert dlg = new Alert(Alert.AlertType.ERROR, "");
            dlg.setTitle("Wrong input");
            dlg.getDialogPane().setContentText("Teacher information was incorrect");

            dlg.showAndWait();
        }
        tbTeacherName.clear();
        tbTeacherType.clear();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tblTeachers.setItems(Main.context.teacherDao.getData());
    }
}
