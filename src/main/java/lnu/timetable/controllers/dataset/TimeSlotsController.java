package lnu.timetable.controllers.dataset;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import lnu.timetable.main.Main;
import lnu.timetable.models.TimeSpan;
import lnu.timetable.ui.MessageBox;
import org.controlsfx.control.CheckListView;
import org.controlsfx.control.RangeSlider;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Bohdan on 15-Dec-15.
 */
public class TimeSlotsController implements Initializable {

    @FXML
    private TableView<TimeSpan> tblTimeSpans;
    @FXML
    private HBox timeSlotsBottomMenu;
    @FXML
    private TextField timeSpanTextField;
    @FXML
    private VBox checkListViewContainer;

    private RangeSlider timeSpanSlider;
    private CheckListView<String> listDays;

    @FXML
    public void onAddTimePeriodAction(ActionEvent actionEvent) {
        int low = timeSpanSlider.lowValueProperty().intValue();
        int high = timeSpanSlider.highValueProperty().intValue();

        if (low == high) {
            Alert dlg = new Alert(Alert.AlertType.ERROR, "");
            dlg.setTitle("Wrong input");
            dlg.getDialogPane().setContentText("Period start and end can't be equal");

            dlg.showAndWait();
            return;
        }

        tblTimeSpans.getItems().add(new TimeSpan(timeSpanTextField.getText()));
    }

    private RangeSlider createLabelSlider() {
        timeSpanSlider = new RangeSlider(480, 1200, 510, 590);

        timeSpanSlider.setShowTickLabels(true);
        timeSpanSlider.setBlockIncrement(30);
        timeSpanSlider.setPrefWidth(300);
        timeSpanSlider.setMajorTickUnit(30);

        timeSpanSlider.setLabelFormatter(new StringConverter<Number>() {

            @Override
            public String toString(Number object) {
                int minutes = object.intValue();
                return String.format("%02d:%02d", minutes / 60, minutes % 60);
            }

            @Override
            public Number fromString(String string) {
                return Double.valueOf(string);
            }
        });

        timeSpanSlider.setOnMouseDragged(event -> {
            int low = timeSpanSlider.lowValueProperty().intValue();
            low = low - low % 5;
            int high = timeSpanSlider.highValueProperty().intValue();
            high = high - high % 5;
            timeSpanTextField.setText(String.format("%02d:%02d - %02d:%02d", low / 60, low % 60, high / 60, high % 60));
        });

        return timeSpanSlider;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("TimeSlots controller Loaded");

        // bind data
        tblTimeSpans.setItems(Main.context.getTime().getHour());

        listDays = new CheckListView<>();

        // load possible days
        ObservableList<String> days = FXCollections.observableArrayList();
        days.addAll(
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday"
        );
        listDays.setItems(days);

        // check days that is already in data model
        for (String day : Main.context.getTime().getDay()) {
            listDays.getCheckModel().check(day);
        }

        // listen for model changes
        Main.context.getTime().getDay().addListener((ListChangeListener<? super String>) c -> {
            for (String day : c.getList()) {
                if (!listDays.getCheckModel().isChecked(day))
                    listDays.getCheckModel().check(day);
            }
        });

        // notify model, if data is changed from view
        listDays.getCheckModel().getCheckedItems().addListener((ListChangeListener<String>) c -> {
            while (c.next()) {
                if (c.wasRemoved()) {
                    String removed = c.getRemoved().get(0);

                    if (c.getList().size() == 0) {
                        MessageBox.showAndWait("Alert", "Notice: You can't generate timetable with no days", Alert.AlertType.WARNING);
                    }
                    Main.context.getTime().getDay().remove(removed);

                } else if (c.wasAdded()) {
                    String added = c.getAddedSubList().get(0);
                    if (!Main.context.getTime().getDay().contains(added)) {
                        Main.context.getTime().getDay().add(added);
                    }
                }
            }
        });

        // create bottom menu
        timeSlotsBottomMenu.getChildren().add(0, createLabelSlider());

        int low = timeSpanSlider.lowValueProperty().intValue();
        low = low - low % 5;
        int high = timeSpanSlider.highValueProperty().intValue();
        high = high - high % 5;
        timeSpanTextField.setText(String.format("%02d:%02d - %02d:%02d", low / 60, low % 60, high / 60, high % 60));

        checkListViewContainer.getChildren().add(listDays);
        checkListViewContainer.setVgrow(listDays, Priority.ALWAYS);
    }
}
