package lnu.timetable.controllers;

import de.jensd.shichimifx.utils.SplitPaneDividerSlider;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import lnu.timetable.io.Export;
import lnu.timetable.io.Import;
import lnu.timetable.main.Main;
import lnu.timetable.persistence.context.TimetableDataContext;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Bohdan on 30-Nov-15.
 */
public class MainController implements Initializable {

    private double xOffset;
    private double yOffset;

    private SplitPaneDividerSlider splitPaneDividerSlider;

    @FXML
    private ToggleButton toggleMenuButton;
    @FXML
    private BorderPane mainBorderPane;
    @FXML
    private SplitPane splitPane;

    @FXML
    public void onBtnDataAction(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/dataset/dataset.fxml"));
            mainBorderPane.setCenter(root);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onBtnActivitiesAction(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/activities.fxml"));
            mainBorderPane.setCenter(root);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onBtnConstraintsAction(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/constraints.fxml"));
            mainBorderPane.setCenter(root);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onBtnTimetablesAction(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/timetable.fxml"));
            mainBorderPane.setCenter(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onMenuOpenAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Timetable Data File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("XML", "*.xml")
        );

        File f = fileChooser.showOpenDialog(Main.primaryStage);


        if (f != null) Import.readData(f.getAbsolutePath());
    }

    @FXML
    public void onMenuSaveAction(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Timetable row data");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("XML", "*.xml")
        );
        File file = fileChooser.showSaveDialog(Main.primaryStage);

        Export export = new Export();
        if (file != null) export.SaveKB(file.getAbsolutePath(), Main.context);
    }

    @FXML
    public void onMenuNewAction(ActionEvent actionEvent) {
        Main.context = new TimetableDataContext();
        Main.generator.SetData(Main.context);

        try {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/dataset/dataset.fxml"));
            mainBorderPane.setCenter(root);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onCloseButtonAction(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    public void onMenuMouseClicked(MouseEvent event) {
        if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2) {
            Main.primaryStage.setMaximized(!Main.primaryStage.isMaximized());
        }
    }

    @FXML
    public void onMenuMouseDragged(MouseEvent event) {
        mainBorderPane.getScene().getWindow().setX(event.getScreenX() - xOffset);
        mainBorderPane.getScene().getWindow().setY(event.getScreenY() - yOffset);
    }

    @FXML
    public void onMenuMousePressed(MouseEvent event) {
        xOffset = event.getSceneX();
        yOffset = event.getSceneY();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        assert mainBorderPane != null : "fx:id=\"mainBorderPane\" was not injected";

        splitPaneDividerSlider = new SplitPaneDividerSlider(splitPane, 0, SplitPaneDividerSlider.Direction.LEFT);
        toggleMenuButton.selectedProperty().bindBidirectional(splitPaneDividerSlider.aimContentVisibleProperty());

        try {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/dataset/dataset.fxml"));
            mainBorderPane.setCenter(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
