package lnu.timetable.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import lnu.timetable.main.Main;
import lnu.timetable.models.Activity;
import lnu.timetable.models.Student;
import lnu.timetable.models.Subject;
import lnu.timetable.models.Teacher;
import lnu.timetable.persistence.context.TimetableDataContext;
import lnu.timetable.ui.MessageBox;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Bohdan on 15-Dec-15.
 */
public class ActivitiesController implements Initializable {

    @FXML
    private TableView<Activity> tblActivities;
    @FXML
    private Accordion accordion;

    @FXML
    private ComboBox<Student> comboBoxStudents;
    @FXML
    private ComboBox<Subject> comboBoxSubjectForStudent;
    @FXML
    private ComboBox<String> comboBoxSubjectTypeForStudent;

    @FXML
    private TextArea textAreaStudentSubject;
    @FXML
    private TextArea textAreaTeacherSubject;

    @FXML
    private ComboBox<Teacher> comboBoxTeachers;
    @FXML
    private ComboBox<Subject> comboBoxSubjectForTeacher;
    @FXML
    private ComboBox<String> comboBoxSubjectTypeForTeacher;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        accordion.setExpandedPane(accordion.getPanes().get(0));

        tblActivities.setItems(Main.context.activityDao.getData());

        TimetableDataContext context = Main.context;

        ObservableList<String> subjectTypes = FXCollections.observableArrayList("Theory", "Practical");

        comboBoxStudents.setItems(context.studentDao.getData());
        comboBoxSubjectForStudent.setItems(context.subjectDao.getData());
        comboBoxSubjectTypeForStudent.setItems(subjectTypes);

        comboBoxTeachers.setItems(context.teacherDao.getData());
        comboBoxSubjectForTeacher.setItems(context.subjectDao.getData());
        comboBoxSubjectTypeForTeacher.setItems(subjectTypes);

        comboBoxSubjectForStudent.getSelectionModel()
                .selectedItemProperty()
                .addListener((o, oldVal, newVal) -> updateStudentSubjectData());

        comboBoxSubjectForTeacher.getSelectionModel()
                .selectedItemProperty()
                .addListener((o, oldVal, newVal) -> updateTeacherSubjectData());
    }

    private void updateTeacherSubjectData() {
        Subject subject = comboBoxSubjectForTeacher.getValue();
        if (subject == null) return;

        int id = Main.context.assignSubjectsDataSet.searchAssignedSubject(subject.getName());

        String text = Main.context.assignSubjectsDataSet.getSubjectsTHRecord(id);
        text += Main.context.assignSubjectsDataSet.getSubjectsPRRecord(id);

        textAreaTeacherSubject.setText(text.replace("[", "\n").replace("]", "."));
    }

    private void updateStudentSubjectData() {
        Subject subject = comboBoxSubjectForStudent.getValue();
        if (subject == null) return;

        int id = Main.context.assignSubjectsDataSet.searchAssignedSubject(subject.getName());

        String text = Main.context.assignSubjectsDataSet.getSubjectsTHRecord(id);
        text += Main.context.assignSubjectsDataSet.getSubjectsPRRecord(id);

        textAreaStudentSubject.setText(text.replace("[", "\n").replace("]", "."));
    }


    public void onBtnGenerateActivitiesAction(ActionEvent actionEvent) {
        TimetableDataContext context = Main.context;

        Alert dlg = new Alert(Alert.AlertType.INFORMATION, "");
        dlg.setTitle("Generation result");

        if (context.assignSubjectsDataSet.isUptodate()) {

            Main.generator.SetData(context);
            Main.generator.GenerateActivities();

            if (Main.generator.isActivitiesGenerated()) {
                dlg.getDialogPane().setContentText("Activities Generated ");

            } else {
                dlg.getDialogPane().setContentText("Error: No Activities Generated");
            }
        } else {
            dlg.getDialogPane().setContentText("Error: No Activities Generated");
        }

        dlg.showAndWait();
    }

    public void onAssignTeacherToSubjectAction(ActionEvent actionEvent) {
        // if (selSubSet != null) {
        Subject subject = comboBoxSubjectForTeacher.getSelectionModel().getSelectedItem();
        Teacher teacher = comboBoxTeachers.getSelectionModel().getSelectedItem();
        String subjectType = comboBoxSubjectTypeForTeacher.getSelectionModel().getSelectedItem();

        if (subject == null || teacher == null || subjectType == null) {
            MessageBox.showAndWait("Wrong input", "Select all data", Alert.AlertType.ERROR);
            return;
        }

        int i1 = Main.context.assignSubjectsDataSet.searchAssignedSubject(subject.getName());
        assignTchSub_Func(i1, teacher.getName(), subjectType);

    }

    private void assignTchSub_Func(int i, String s, String subjectType) {

        switch (subjectType) {
            case "Theory":
                Main.context.assignSubjectsDataSet.getAssignedSubject(i).setTchTH(s);

                break;
            case "Practical":

                int slimit = Main.context.assignSubjectsDataSet.getAssignedSubject(i).getStudPR().size();

                if (1 > slimit) {
                    MessageBox.showAndWait("Wrong input",
                            "Invalid Selection" + "Must Select less than " + slimit + " Teachers",
                            Alert.AlertType.ERROR);
                } else {
                    Main.context.assignSubjectsDataSet.getAssignedSubject(i).clearTchPR();
                    Main.context.assignSubjectsDataSet.getAssignedSubject(i).getTchPR().add(0, s);

                }
                break;
            default:
                MessageBox.showAndWait("Wrong input", "Select subject type first", Alert.AlertType.ERROR);
                break;
        }

        Main.context.assignSubjectsDataSet.copySubjToAssignSubj();
        Main.context.assignSubjectsDataSet.updateAssignSubj();
    }

    public void onAssignStudentToSubjectAction(ActionEvent actionEvent) {
        Subject subject = comboBoxSubjectForStudent.getSelectionModel().getSelectedItem();
        Student student = comboBoxStudents.getSelectionModel().getSelectedItem();
        String subjectType = comboBoxSubjectTypeForStudent.getSelectionModel().getSelectedItem();

        if (subject == null || student == null || subjectType == null) {
            MessageBox.showAndWait("Wrong input", "Select all data", Alert.AlertType.ERROR);
            return;
        }

        int i1 = Main.context.assignSubjectsDataSet.searchAssignedSubject(subject.getName());
        assignStudSub_Func(i1, student.getName(), subjectType);

        updateStudentSubjectData();
    }

    private void assignStudSub_Func(int i, String s, String subjectType) {

        switch (subjectType) {
            case "Theory":

                Main.context.assignSubjectsDataSet.getAssignedSubject(i).setStudTH(s);

                break;
            case "Practical":

                Main.context.assignSubjectsDataSet.getAssignedSubject(i).clearStudPR();
                Student stud = Main.context.studentDao.get(Main.context.studentDao.search(s));

                if (stud.getGroupList().size() > 0) {
                    for (int j = 0; j < stud.getGroupList().size(); j++) {
                        s = stud.getGroupName(j);
                        Main.context.assignSubjectsDataSet.getAssignedSubject(i).getStudPR().add(j, s);

                    }
                } else
                    MessageBox.showAndWait("Group creation Error", "The groups are not created!", Alert.AlertType.ERROR);

                break;
            default:
                MessageBox.showAndWait("Wrong input", "Select subject type first", Alert.AlertType.ERROR);
                break;
        }

        Main.context.assignSubjectsDataSet.copySubjToAssignSubj();
        Main.context.assignSubjectsDataSet.updateAssignSubj();
    }
}
