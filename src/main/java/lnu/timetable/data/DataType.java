package lnu.timetable.data;

/**
 * Created by Bohdan on 03-Dec-15.
 */
public class DataType {
    public static final int DATA_NAME = 0;          // name field
    public static final int DATA_TYPE = 1;          // type field
    public static final int DATA_CAPACITY = 2;      // capacity field
    public static final int DATA_NOT = 3;           // no. of theory field
    public static final int DATA_NOP = 4;           // no. of practical field
    public static final int DATA_STRENGTH = 5;      // no. of student field
}
