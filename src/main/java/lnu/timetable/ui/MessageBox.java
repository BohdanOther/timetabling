package lnu.timetable.ui;

import javafx.scene.control.Alert;

/**
 * Created by Bohdan on 16-Dec-15.
 */
public class MessageBox {

    public static void show(String title, String content, Alert.AlertType type){
        Alert dlg = new Alert(type, content);
        dlg.setTitle("Wrong input");
        dlg.show();
    }

    public static void showAndWait(String title, String content, Alert.AlertType type){
        Alert dlg = new Alert(type, content);
        dlg.setTitle("Wrong input");
        dlg.showAndWait();
    }
}
