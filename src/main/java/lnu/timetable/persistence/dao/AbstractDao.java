package lnu.timetable.persistence.dao;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lnu.timetable.models.Entity;

import javax.swing.*;

/**
 * Created by Bohdan on 30-Nov-15.
 */
public abstract class AbstractDao<T extends Entity> implements Dao<T> {

    protected ObservableList<T> data;

    @Override
    public ObservableList<T> getData() {
        return data;
    }

    private boolean upToDate;

    public AbstractDao() {
        data = FXCollections.observableArrayList();
    }

    @Override
    public int search(String name) {
        for (int i = 0; i < data.size(); i++)
            if (data.get(i).getName().equalsIgnoreCase(name))
                return i;
        return -1;
    }


    @Override
    public T get(int index) {
        return data.get(index);
    }

    @Override
    public void add(T item) {
        if (search(item.getName()) == -1) {
            data.add(item);
            setStatus(false);
        } else
            JOptionPane.showMessageDialog(null, "Duplicate Entry : " + item.getName(),
                    "Duplicate", JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void remove(int pos) {
        data.remove(pos);
    }

    @Override
    public boolean getStatus() {
        return upToDate;
    }

    @Override
    public void setStatus(boolean status) {
        upToDate = status;
    }

    @Override
    public int length() {
        return data.size();
    }

}
