package lnu.timetable.persistence.dao;

import lnu.timetable.models.Student;

import javax.swing.*;
import java.util.Vector;

/**
 * Created by Bohdan on 30-Nov-15.
 */
public class StudentDao extends AbstractDao<Student> {

    public Vector<String> getStudentNames() {
        Vector<String> names = new Vector<>();
        for (int i = 0, k = 0; i < length(); i++) {
            names.add(k++, get(i).getName());
            for (int j = 0; j < get(i).getGroupList().size(); j++)
                names.add(k++, get(i).getGroupName(j));
        }
        return names;
    }

    public int searchGroupStudent(int i, String name) {
        Student temp = get(i);
        for (int j = 0; j < temp.getGroupList().size(); j++)
            if (temp.getGroupList().get(j).getName().equalsIgnoreCase(name))
                return j;
        return -1;
    }

    public Student searchGroupStudentSet(Student temp, String name) {
        for (int i = 0; i < temp.getGroupList().size(); i++)
            if (temp.getGroupList().get(i).getName().equalsIgnoreCase(name))
                return temp.getGroupList().get(i);
        return null;
    }

    public void removeGroup(int pos, int gpos) {
        int gs = data.get(pos).getGroupSize(gpos);
        data.get(pos).setGroupLimit(gs);
        data.get(pos).getGroupList().remove(gpos);

    }

    public boolean addGroup(String Name, String GName, int Strength) {
        int i = search(Name);
        Student temp = get(i);
        if (temp != null) {
            System.out.println(temp.getTotStud() + ":" + temp.getGroupLimit());
            if (searchGroupStudent(i, GName) == -1) {
                if (Strength <= (temp.getTotStud() - temp.getGroupLimit())) {
                    temp.getGroupList().add(new Student(GName, Strength));
                    temp.setGroupLimit(Strength);
                    setStatus(false);
                    return true;
                } else
                    JOptionPane
                            .showMessageDialog(null,
                                    "The Group Size for Group"
                                            + " \n must be less than "
                                            + (temp.getTotStud() - temp
                                            .getGroupLimit()),
                                    "Group Size Exceeds!",
                                    JOptionPane.ERROR_MESSAGE);
            } else
                JOptionPane.showMessageDialog(null, "Duplicate Entry : "
                        + GName, "Duplicate", JOptionPane.INFORMATION_MESSAGE);

        } else
            JOptionPane.showMessageDialog(null, Name
                            + " not avail!\n The Group can't create", "Group Error!",
                    JOptionPane.ERROR_MESSAGE);
        return false;
    }

    public int getAllStudentLength() {
        int size = 0;
        for (Student aStudentList : data) {
            size += aStudentList.getGroupList().size() + 1;
        }
        return size;
    }

    @Override
    public Object[] getRecord(int i) {
        return new Object[]{data.get(i).getName(),
                data.get(i).getTotStud()};
    }

    public Object[] getGroupStudentRecord(int i, int j) {
        return new Object[]{data.get(i).getGroupList().get(j).getName(),
                data.get(i).getGroupList().get(j).getTotStud()};
    }
}
