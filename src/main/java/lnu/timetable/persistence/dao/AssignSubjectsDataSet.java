package lnu.timetable.persistence.dao;

import lnu.timetable.models.AssignSubjects;

import java.util.ArrayList;

/**
 * Created by Bohdan on 30-Nov-15.
 */
public class AssignSubjectsDataSet {

    private ArrayList<AssignSubjects> assignSubjectses;

    StudentDao studentDataSet;
    TeacherDao teacherDataSet;
    SubjectDao subjectDataSet;

    public AssignSubjectsDataSet(StudentDao students, TeacherDao teachers, SubjectDao subjects){
        studentDataSet = students;
        teacherDataSet = teachers;
        subjectDataSet = subjects;

        assignSubjectses = new ArrayList<>();
    }

    public ArrayList<AssignSubjects> getAssignedSubjectList() {
        return assignSubjectses;
    }

    public AssignSubjects getAssignedSubject(int i) {
        return assignSubjectses.get(i);
    }

    public int searchAssignedSubject(String name) {
        for (int i = 0; i < getAssignedSubjectLength(); i++)
            if (assignSubjectses.get(i).getSubj().getName().equals(name))
                return i;
        return -1;
    }

    public int getAssignedSubjectLength() {
        return assignSubjectses.size();
    }

    public String getSubjectsTHRecord(int i) {
        String obj = "[Subject : " + assignSubjectses.get(i).getSubj().getName()
                + "]";
        obj += "[TH: ";
        obj += "[Teacher : " + assignSubjectses.get(i).getTchTH() + "]";
        obj += "[Students : " + assignSubjectses.get(i).getStudTH() + "]";
        return obj;
    }

    public String getSubjectsPRRecord(int i) {
        String obj = "[PR: ";

        for (int j = 0; j < assignSubjectses.get(i).getTchPR().size(); j++)
            obj += "[Teacher : " + assignSubjectses.get(i).getTchPR().get(j) + "]";

        for (int j = 0; j < assignSubjectses.get(i).getStudPR().size(); j++)
            obj += "[Students : " + assignSubjectses.get(i).getStudPR().get(j)
                    + "]";
        return obj;
    }

    public void copySubjToAssignSubj() {

        for (int i = 0; i < subjectDataSet.length(); i++) {
            if (assignSubjectses.size() <= i) {
                AssignSubjects temp = new AssignSubjects();
                temp.setSubj(subjectDataSet.get(i));
                assignSubjectses.add(i, temp);
                System.out.println("i::" + i);
            }
        }
    }

    public void updateAssignSubj() {
        for (int i = 0; i < getAssignedSubjectLength(); i++) {
            AssignSubjects temp = assignSubjectses.get(i);

            if (subjectDataSet.search(temp.getSubj().getName()) == -1)
                assignSubjectses.remove(i);
            else {

                if (temp.getStudTH() != null)
                    if (studentDataSet.search(temp.getStudTH()) == -1) {
                        update(temp.getStudPR(), 0);
                        temp.setStudTH(null);
                    }

                if (temp.getTchTH() != null)
                    if (teacherDataSet.search(temp.getTchTH()) == -1)
                        temp.setTchTH(null);
                update(temp.getTchPR(), 1);
            }
        }
    }

    public void update(ArrayList<String> str, int t) {
        for (int i = 0; i < str.size(); i++) {
            switch (t) {
                case 0:
                    str.clear();
                    break;
                case 1:
                    if (teacherDataSet.search(str.get(i)) == -1)
                        str.remove(i);
                    break;
            }
        }
    }

    public boolean isUptodate() {
        if (getAssignedSubjectLength() == 0)
            return false;
        for (int i = 0; i < getAssignedSubjectLength(); i++) {
            AssignSubjects temp = assignSubjectses.get(i);
            int index = subjectDataSet.search(temp.getSubj().getName());

            if (index == -1)
                return false;
            else {
                if (subjectDataSet.get(index).getNoTheory() > 0)
                    if ((temp.getStudTH() == null) || (temp.getTchTH() == null))
                        return false;

                if (temp.getStudTH() != null)
                    if (studentDataSet.search(temp.getStudTH()) == -1)
                        return false;
                if (temp.getTchTH() != null)
                    if (teacherDataSet.search(temp.getTchTH()) == -1)
                        return false;
            }
        }
        return true;
    }
}
