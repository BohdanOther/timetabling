package lnu.timetable.persistence.dao;


import lnu.timetable.models.Teacher;

/**
 * Created by Bohdan on 30-Nov-15.
 */
public class TeacherDao extends AbstractDao<Teacher> {

    @Override
    public Object[] getRecord(int i) {
        return new Object[]{data.get(i).getName(),
                data.get(i).getType()};
    }
}
