package lnu.timetable.persistence.dao;


import lnu.timetable.models.Room;

/**
 * Created by Bohdan on 30-Nov-15.
 */
public class RoomDao extends AbstractDao<Room> {

    @Override
    public Object[] getRecord(int i) {
        return new Object[]{data.get(i).getName(), data.get(i).getType(),
                data.get(i).getCapacity()};
    }
}
