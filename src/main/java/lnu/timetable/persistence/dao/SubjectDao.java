package lnu.timetable.persistence.dao;

import lnu.timetable.models.Subject;


/**
 * Created by Bohdan on 30-Nov-15.
 */
public class SubjectDao extends AbstractDao<Subject> {

    @Override
    public Object[] getRecord(int i) {
        return new Object[]{data.get(i).getName(),
                data.get(i).getNoTheory(), data.get(i).getNoPractice()};
    }
}
