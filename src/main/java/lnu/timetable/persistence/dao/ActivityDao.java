package lnu.timetable.persistence.dao;


import lnu.timetable.models.Activity;

/**
 * Created by Bohdan on 30-Nov-15.
 */
public class ActivityDao extends AbstractDao<Activity> {

    public void addActivity(int i, Activity act) {
        data.add(i, act);
        setStatus(false);
    }

    public Object[] getRecord(int i) {

        return new Object[]{"ID :" + data.get(i).getId(),
                "Tag :" + data.get(i).getTag(),
                "Subject :" + data.get(i).getSubject(),
                "Student :" + data.get(i).getStudent(),
                "Teacher :" + data.get(i).getTeacher(),
                "Duration :" + data.get(i).getDuration()
        };
    }
}
