package lnu.timetable.persistence.dao;

import javafx.collections.ObservableList;
import lnu.timetable.models.Entity;

/**
 * Created by Bohdan on 09-Dec-15.
 */
public interface Dao<T extends Entity> {

    ObservableList<T> getData();

    int search(String name);

    T get(int index);

    void add(T item);

    void remove(int pos);

    boolean getStatus();

    void setStatus(boolean status);

    int length();

    Object[] getRecord(int i);
}
