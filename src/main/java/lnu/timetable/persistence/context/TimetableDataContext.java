package lnu.timetable.persistence.context;

import lnu.timetable.algorithm.Rules;
import lnu.timetable.models.Time;
import lnu.timetable.persistence.dao.*;

import java.util.Vector;

/**
 * Created by Bohdan on 03-Dec-15.
 */
public class TimetableDataContext {

    public StudentDao studentDao;
    public TeacherDao teacherDao;
    public SubjectDao subjectDao;
    public RoomDao roomDao;
    public ActivityDao activityDao;

    public AssignSubjectsDataSet assignSubjectsDataSet;

    private Rules ruleSet;
    private Time time;

    private int TH, PR;

    public TimetableDataContext() {

        time = new Time();
        ruleSet = new Rules();

        studentDao = new StudentDao();
        teacherDao = new TeacherDao();
        subjectDao = new SubjectDao();
        roomDao = new RoomDao();
        activityDao = new ActivityDao();

        assignSubjectsDataSet = new AssignSubjectsDataSet(studentDao, teacherDao, subjectDao);
    }

    public void setPR(int practice) {
        this.PR = practice;
    }

    public int getPR() {
        return PR;
    }

    public void setTH(int theory) {
        TH = theory;
    }

    public int getTH() {
        return TH;
    }

    public Time getTime() {
        return time;
    }

    public Rules getRule() {
        return ruleSet;
    }
}
