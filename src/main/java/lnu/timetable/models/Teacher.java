package lnu.timetable.models;

public class Teacher extends Entity {

    private String type;

    public Teacher(String Name, String Type) {
        super(Name);
        type = Type;
    }

    public String getType() {
        return type;
    }
    public void setType(String s) {
        type = s;
    }
}
