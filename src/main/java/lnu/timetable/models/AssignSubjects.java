package lnu.timetable.models;

import java.util.ArrayList;

public class AssignSubjects {

    private ArrayList<String> studNamePR;
    private ArrayList<String> tchNamePR;
    private ArrayList<String> roomNamePR;

    private String roomNameTH;
    private String studNameTH;
    private String tchNameTH;
    private Subject subj;

    public AssignSubjects() {
        studNamePR = new ArrayList<>();
        tchNamePR = new ArrayList<>();
        roomNamePR = new ArrayList<>();
    }

    public Subject getSubj() {
        return subj;
    }

    public void setSubj(Subject subj) {
        this.subj = subj;
    }

    public String getRoomTH() {
        return roomNameTH;
    }
    public void setRoomTH(String s) {
        roomNameTH = s;
    }
    public String getStudTH() {
        return studNameTH;
    }

    public void setStudTH(String s) {
        studNameTH = s;
    }
    public String getTchTH() {
        return tchNameTH;
    }
    public void setTchTH(String s) {
        tchNameTH = s;
    }

    public ArrayList<String> getRoomPR() {
        return roomNamePR;
    }
    public ArrayList<String> getStudPR() {
        return studNamePR;
    }
    public ArrayList<String> getTchPR() {
        return tchNamePR;
    }

    public void clearRoomPR() {
        roomNamePR.clear();
    }
    public void clearStudPR() {
        studNamePR.clear();
    }
    public void clearTchPR() {
        tchNamePR.clear();
    }
}
