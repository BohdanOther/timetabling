package lnu.timetable.models;

import javafx.beans.property.SimpleBooleanProperty;

import java.util.Vector;

/**
 * Created by Bohdan on 17-Dec-15.
 */
public class ConstraintEntry {

    public boolean getByName(String name) {
        if (name.equals("Monday")) return getMonday();
        if (name.equals("Tuesday")) return getTuesday();
        if (name.equals("Wednesday")) return getWednesday();
        if (name.equals("Thursday")) return getThursday();
        if (name.equals("Friday")) return getFriday();
        if (name.equals("Saturday")) return getSaturday();
        System.out.println(name);
        return false;
    }

    public boolean getByNumber(int n) {
        switch (n) {
            case 0:
                return getMonday();
            case 1:
                return getTuesday();
            case 2:
                return getWednesday();
            case 3:
                return getThursday();
            case 4:
                return getFriday();
            case 5:
                return getSaturday();
        }
        return false;
    }

    private String timeSlot;

    private SimpleBooleanProperty monday = new SimpleBooleanProperty(true);
    private SimpleBooleanProperty tuesday = new SimpleBooleanProperty(true);
    private SimpleBooleanProperty wednesday = new SimpleBooleanProperty(true);
    private SimpleBooleanProperty thursday = new SimpleBooleanProperty(true);
    private SimpleBooleanProperty friday = new SimpleBooleanProperty(true);
    private SimpleBooleanProperty saturday = new SimpleBooleanProperty(true);

    public ConstraintEntry(String time, boolean allVal) {
        timeSlot = time;
        monday.set(allVal);
        tuesday.set(allVal);
        wednesday.set(allVal);
        thursday.set(allVal);
        friday.set(allVal);
        saturday.set(allVal);
    }

    public ConstraintEntry(String timeSlot, Vector<Boolean> data) {

        this.timeSlot = timeSlot;

        int size = data.size();
        if (size > 0) monday.set(data.get(0));
        if (size > 1) tuesday.set(data.get(1));
        if (size > 2) wednesday.set(data.get(2));
        if (size > 3) thursday.set(data.get(3));
        if (size > 4) friday.set(data.get(4));
        if (size > 5) saturday.set(data.get(5));
//
//        ObservableList<String> days = Main.context.getTime().getDay();
//        for (int i = 0; i < days.size(); i++) {
//            if (i >= data.size()) break;
//            if (days.get(i).equals("Monday")) monday.set(data.get(i));
//            if (days.get(i).equals("Tuesday")) tuesday.set(data.get(i));
//            if (days.get(i).equals("Wednesday")) wednesday.set(data.get(i));
//            if (days.get(i).equals("Thursday")) thursday.set(data.get(i));
//            if (days.get(i).equals("Friday")) friday.set(data.get(i));
//            if (days.get(i).equals("Saturday")) saturday.set(data.get(i));
//        }
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public boolean getMonday() {
        return monday.get();
    }

    public SimpleBooleanProperty mondayProperty() {
        return monday;
    }

    public void setMonday(boolean monday) {
        this.monday.set(monday);
    }

    public boolean getTuesday() {
        return tuesday.get();
    }

    public SimpleBooleanProperty tuesdayProperty() {
        return tuesday;
    }

    public void setTuesday(boolean tuesday) {
        this.tuesday.set(tuesday);
    }

    public boolean getWednesday() {
        return wednesday.get();
    }

    public SimpleBooleanProperty wednesdayProperty() {
        return wednesday;
    }

    public void setWednesday(boolean wednesday) {
        this.wednesday.set(wednesday);
    }

    public boolean getThursday() {
        return thursday.get();
    }

    public SimpleBooleanProperty thursdayProperty() {
        return thursday;
    }

    public void setThursday(boolean thursday) {
        this.thursday.set(thursday);
    }

    public boolean getFriday() {
        return friday.get();
    }

    public SimpleBooleanProperty fridayProperty() {
        return friday;
    }

    public void setFriday(boolean friday) {
        this.friday.set(friday);
    }

    public boolean getSaturday() {
        return saturday.get();
    }

    public SimpleBooleanProperty saturdayProperty() {
        return saturday;
    }

    public void setSaturday(boolean saturday) {
        this.saturday.set(saturday);
    }
}
