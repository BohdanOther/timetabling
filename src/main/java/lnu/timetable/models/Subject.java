package lnu.timetable.models;

public class Subject extends Entity {

    private int noPractice = 0;
    private int noTheory = 0;

    public Subject(String Name, int noT, int noP) {
        super(Name);
        noPractice = noP;
        noTheory = noT;

    }

    public int getNoPractice() {
        return noPractice;
    }
    public void setNoPractice(int i) {
        noPractice = i;
    }

    public int getNoTheory() {
        return noTheory;
    }
    public void setNoTheory(int i) {
        noTheory = i;
    }
}