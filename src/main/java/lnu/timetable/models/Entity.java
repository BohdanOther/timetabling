package lnu.timetable.models;

/**
 * Created by Bohdan on 30-Nov-15.
 */
public abstract class Entity {

    private String name;

    public Entity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getName();
    }
}
