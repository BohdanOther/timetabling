package lnu.timetable.models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Time {

    private ObservableList<String> dayList;
    private ObservableList<TimeSpan> hourList;

    public Time() {
        dayList = FXCollections.observableArrayList();
        hourList = FXCollections.observableArrayList();
    }

    public ObservableList<String> getDay() {
        return dayList;
    }

    public ObservableList<TimeSpan> getHour() {
        return hourList;
    }
}
