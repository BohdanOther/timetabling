package lnu.timetable.models;

public class Room extends Entity {

    private int capacity;
    private String type;

    public Room(String Name, int Capacity, String Type) {
        super(Name);
        type = Type;
        capacity = Capacity;
    }

    public String getType() {
        return type;
    }
    public void setType(String s) {
        type = s;
    }

    public int getCapacity() {
        return capacity;
    }
    public void setCapacity(int i) {
        capacity = i;
    }
}
