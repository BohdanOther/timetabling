package lnu.timetable.models;

/**
 * Created by Bohdan on 17-Dec-15.
 */
public class TimeSpan {
    private String span;

    public TimeSpan(String span) {
        this.span = span;
    }

    public String getSpan() {
        return span;
    }

    public void setSpan(String span) {
        this.span = span;
    }

    @Override
    public String toString() {
        return span;
    }
}
