package lnu.timetable.models;

import javafx.scene.control.TableCell;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.util.Callback;

/**
 * Created by Bohdan on 17-Dec-15.
 */
public class CheckBoxCellFactory implements Callback {
    @Override
    public TableCell call(Object param) {
        CheckBoxTableCell<ConstraintEntry, Boolean> checkBoxCell = new CheckBoxTableCell<>();
        return checkBoxCell;
    }
}