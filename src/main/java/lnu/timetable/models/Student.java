package lnu.timetable.models;

import javax.swing.*;
import java.util.ArrayList;

public class Student extends Entity {

    private int totStud;
    private int groupLimit;
    private ArrayList<Student> groupList;

    public Student(String Name, int Strength) {
        super(Name);

        totStud = Strength;
        groupLimit = 0;
        groupList = new ArrayList<>();
    }

    public ArrayList<Student> getGroupList() {
        return groupList;
    }

    public int getTotStud() {
        return totStud;
    }
    public void setTotStud(int size) {
        if (size < totStud)
            alterGroupSize(totStud - size);
        totStud = size;
    }

    private void alterGroupSize(int d) {
        for (Student aGroupList : groupList) {
            int s = aGroupList.getTotStud();
            if ((s - d) > 0) {
                aGroupList.setTotStud(s - d);
                return;
            }
        }
    }

    public int getGroupLimit() {
        return groupLimit;
    }
    public void setGroupLimit(int i) {
        groupLimit += i;
    }

    public String getGroupName(int gno) {
        return groupList.get(gno).getName();
    }
    public void setGroupName(int gno, String s) {
        groupList.get(gno).setName(s);
    }

    public int getGroupSize(int gno) {
        return groupList.get(gno).getTotStud();
    }
    public void setGroupSize(int gno, int i) {
        if (i != getGroupSize(gno)) {
            if (i <= (totStud - groupLimit + getGroupSize(gno))) {
                setGroupLimit(i - getGroupSize(gno));
                groupList.get(gno).setTotStud(i);
            } else
                JOptionPane.showMessageDialog(null, "The Group Size for Group:"
                                + gno + " \n must be less than Strength:"
                                + (totStud - groupLimit + getGroupSize(gno)),
                        "Group Size Exceeds!", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
