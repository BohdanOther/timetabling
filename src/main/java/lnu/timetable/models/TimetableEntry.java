package lnu.timetable.models;

import java.util.Vector;

/**
 * Created by Bohdan on 16-Dec-15.
 */
public class TimetableEntry {

    private String timeSlot;
    private String monday;
    private String tuesday;
    private String wednesday;
    private String thursday;
    private String friday;
    private String saturday;

    public TimetableEntry(String timeSlot, Vector<String> data) {
        this.timeSlot = timeSlot;

        int size = data.size();
        monday      = size > 0 ? data.get(0) : " - x - ";
        tuesday     = size > 1 ? data.get(1) : " - x - ";
        wednesday   = size > 2 ? data.get(2) : " - x - ";
        thursday    = size > 3 ? data.get(3) : " - x - ";
        friday      = size > 4 ? data.get(4) : " - x - ";
        saturday    = size > 5 ? data.get(5) : " - x - ";


//        ObservableList<String> days = Main.context.getTime().getDay();
//        for (int i = 0; i < days.size(); i++) {
//            if (days.get(i).equals("Monday")) monday = data.get(i);
//            if (days.get(i).equals("Tuesday")) tuesday = data.get(i);
//            if (days.get(i).equals("Wednesday")) wednesday = data.get(i);
//            if (days.get(i).equals("Thursday")) thursday = data.get(i);
//            if (days.get(i).equals("Friday")) friday = data.get(i);
//            if (days.get(i).equals("Saturday")) saturday = data.get(i);
//        }
    }

        public TimetableEntry(String timeSlot, String monday) {
            this.timeSlot = timeSlot;
            this.monday = monday;
            this.tuesday = "-/-";
            this.wednesday = "-/-";
            this.thursday = "-/-";
            this.friday = "-/-";
            this.saturday = "-/-";
        }

        public TimetableEntry(String timeSlot, String monday, String tuesday) {
            this.timeSlot = timeSlot;
            this.monday = monday;
            this.tuesday = tuesday;
            this.wednesday = "-/-";
            this.thursday = "-/-";
            this.friday = "-/-";
            this.saturday = "-/-";
        }

        public TimetableEntry(String timeSlot, String monday, String tuesday, String wednesday) {
            this.timeSlot = timeSlot;
            this.monday = monday;
            this.tuesday = tuesday;
            this.wednesday = wednesday;
            this.thursday = "-/-";
            this.friday = "-/-";
            this.saturday = "-/-";
        }

        public TimetableEntry(String timeSlot, String monday, String tuesday, String wednesday, String thursday) {
            this.timeSlot = timeSlot;
            this.monday = monday;
            this.tuesday = tuesday;
            this.wednesday = wednesday;
            this.thursday = thursday;
            this.friday = "-/-";
            this.saturday = "-/-";
        }

        public
        TimetableEntry(String timeSlot, String monday, String tuesday, String wednesday, String thursday, String friday)
        {
            this.timeSlot = timeSlot;
            this.monday = monday;
            this.tuesday = tuesday;
            this.wednesday = wednesday;
            this.thursday = thursday;
            this.friday = friday;
            this.saturday = "-/-";
        }

        public
        TimetableEntry(String timeSlot, String monday, String tuesday, String wednesday, String thursday, String friday, String saturday)
        {
            this.timeSlot = timeSlot;
            this.monday = monday;
            this.tuesday = tuesday;
            this.wednesday = wednesday;
            this.thursday = thursday;
            this.friday = friday;
            this.saturday = saturday;
        }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getMonday() {
        return monday;
    }

    public void setMonday(String monday) {
        this.monday = monday;
    }

    public String getTuesday() {
        return tuesday;
    }

    public void setTuesday(String tuesday) {
        this.tuesday = tuesday;
    }

    public String getWednesday() {
        return wednesday;
    }

    public void setWednesday(String wednesday) {
        this.wednesday = wednesday;
    }

    public String getThursday() {
        return thursday;
    }

    public void setThursday(String thursday) {
        this.thursday = thursday;
    }

    public String getFriday() {
        return friday;
    }

    public void setFriday(String friday) {
        this.friday = friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public void setSaturday(String saturday) {
        this.saturday = saturday;
    }
}
